@extends('admin.layouts.app')
@section('title', 'Order service')
@section('content')
    <div class="inner-block">
        <h2>Đặt chỗ dịch vụ</h2>
        <table class="table table-condensed">
            <thead>
            <tr>
                <th>{{trans('messages.time')}}</th>
                <th>{{trans('messages.name_customer')}}</th>
                <th>{{trans('messages.email')}}</th>
                <th>{{trans('messages.status')}}</th>
                <th width="80px"></th>
            </tr>
            </thead>
            <tbody>
            @forelse($orders as $order)
                <tr style="margin-bottom: 10px;">
                    <td>{{$order->time}}</td>
                    <td>{{$order->name}}</td>
                    <td>{{$order->email}}</td>
                    <td>{{$order->type == \App\OrderService::ORDER ? 'Đang đặt dịch vụ' : $order->type == \App\OrderService::SUCCESS ? 'Đã xác nhận' : 'Đã hủy'}}</td>
                    <td>
                        @if($order->type == \App\OrderService::ORDER)
                            <a href="{{route('book-service.edit', ['id' => $order->id, 'type' => \App\OrderService::SUCCESS])}}" class="btn btn-success " style="margin: 3px;">Xác nhân đặt hàng</a>
                            <a href="{{route('book-service.edit', ['id' => $order->id, 'type' => \App\OrderService::DENY])}}" class="btn btn-danger ">Hủy đơn đặt hàng</a>
                        @elseif($order->type == \App\OrderService::SUCCESS)
                            <a href="{{route('book-service.edit', ['id' => $order->id, 'type' => \App\OrderService::END])}}" class="btn btn-info ">Hoàn tất</a>
                        @endif
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="12"><h2 class="text-center no_data">{{ trans('messages.no_data') }}</h2></td>
                </tr>
            @endforelse
            </tbody>
        </table>
        <!-- pagination -->
        <div class="row text-center">
            {{ $orders->links() }}
        </div>
    </div>
@endsection