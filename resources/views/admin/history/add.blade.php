@extends('admin.layouts.app')
@section('title', 'Add history')
@section('content')
    <div class="inner-block">
        <div class="product-block">
            <div class="pro-head">
                <h2>Thêm lịch sửa sử dungj dịch vụ</h2>
            </div>
            <div class="error">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-md-12 product-grid">
                <form action="{{route('history.add')}}" method="POST" role="form" class="form-add-info" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="">Chọn khách hàng</label>
                        <select class="form-control data-select-all <?php echo $errors->has('user') ? 'input-error' : '';?>" name="user">
                            @if(count($users) > 0)
                                <option value="">Chọn khách hàng</option>
                                @foreach ($users as $user)
                                    <option value="{{$user->id}}" {{old('user') == $user->id ?'selected':''}}>{{$user->name}}</option>
                                @endforeach
                            @else
                                <option value="">Không có khách hàng nào, vui lòng thêm khách hang</option>
                            @endif

                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Chọn dịch vụ</label>
                        <select class="form-control data-select-all <?php echo $errors->has('user') ? 'input-error' : '';?>" name="service">
                            @if(count($users) > 0)
                                <option value="">Chọn dịch vụ</option>
                                @foreach ($services as $service)
                                    <option value="{{$service->id}}" {{old('service') == $service->id ?'selected':''}}>{{$service->name}}</option>
                                @endforeach
                            @else
                                <option value="">Không có dịch vụ nào, vui lòng thêm dịch vụ</option>
                            @endif

                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary submit-form">Thêm</button>
                </form>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection