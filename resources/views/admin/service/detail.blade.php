<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Chi tiết dịch vụ</h4>
        </div>
        <div class="modal-body">
            <p><strong>Tên dịch vụ</strong> {{$service->name}}</p>
            <p><strong>Giá dịch vụ</strong> {{$service->price}} vnđ</p>
            <p><strong>Tiền thưởng</strong> {{$service->bonus}} vnđ</p>
            <p><strong>Thời gian</strong> {{$service->time}} {{$service->unit_time == \App\Service::HOURS ? 'giờ' : $service->unit_time == \App\Service::MINUTE ? 'phút': 'giây'}}</p>
            <p><strong>Mô tả dịch vụ</strong> {{$service->description}}</p>
            <div>
                <p><strong>Hình ảnh</strong></p>
                <img src="{{asset('/storage/service/'.$service->image)}}" alt="{{$service->name}}" style="max-width: 100%;">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('messages.close_lable')}}</button>
        </div>
    </div>
</div>
