@extends('admin.layouts.app')
@section('title', 'service')
@section('content')
    <div class="inner-block">
        <div class="product-block">
            <div class="pro-head">
                <h2>Danh sách dịch vụ
                    <a href="{{route('service.add.form')}}" class="pull-right btn btn-primary">Thêm dịch vụ</a>
                </h2>
            </div>
            @forelse ($services as $service)
                <div class="item  col-xs-6 col-sm-4 col-md-3 service">
                    <div class="thumbnail">
                        <img class="group list-group-image" src="{{asset('/storage/service/'.$service->image)}}" alt="{{$service->name}}" />
                        <div class="caption">
                            <h3 class="group inner list-group-item-heading text-center">{{$service->name}}</h3>
                            <p class="lead text-center">@php echo number_format($service->price, 0 , ',', '.'); @endphp {{trans('messages.money')}}</p>
                            <p>
                                <a class="btn btn-success showCommonModel" data-toggle="modal" data-target="#service_model" href="{{route('service.detail', $service->id)}}">{{trans('messages.detail_lable')}}</a>
                                <a class="btn btn-warning pull-right" href="{{route('service.edit.form', $service->id)}}">{{trans('messages.edit_lable')}}</a>
                            </p>
                        </div>
                    </div>
                </div>
            @empty
                <h1 class="text-center">{{trans('messages.no_data')}}</h1>
            @endforelse
            <div class="col-xs-12 text-center">
                {{ $services->links() }}
            </div>
            <div class="cle arfix"></div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade common_ajax" id="service_model"></div><!-- /.modal -->
@endsection