@extends('frontend.layouts.app')
@section('content')
    <!-- about-top -->
    <div class="agileits-about-top">
        <div class="container">
            <div class="agileinfo-top-grids">
                    <div class="col-sm-12 wthree-top-grid">
                        <h4 class="text-center">{{$service->name}}</h4>
                        <p class="text-center"><img src="{{asset('/storage/service/'.$service->image)}}" class="img-responsive" alt="{{$service->name}}" /></p>
                        <p>
                            {!! $service->description!!}
                        </p>
                    </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <!-- //about-top -->
@endsection