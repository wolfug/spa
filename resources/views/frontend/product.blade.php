@extends('frontend.layouts.app')
@section('title', 'index')
@section('content')
    <!-- gallery -->
    <div class="banner-bottom gallery">
        <div class="container">
            <h3 class="heading-agileinfo">Sản phẩm của chúng tôi<span>Skin,Nails and Beauty Services</span></h3>
            <div class="col-md-12" style="margin-top: 20px;margin-bottom: 30px;">
                <form action="{{route('frontend.product')}}" method="get" role="form" class="form-add-info">
                    <div class="form-group">
                        <div class="col-md-10 pull-left">
                            <input type="text" class="form-control" name="name" value="{{old('name')}}" placeholder="Nhập tên sản phẩm">
                        </div>
                        <div class="col-md-2">
                            <input type="submit" value="tìm kiếm" class="btn btn-info">
                        </div>
                    </div>
                </form>
            </div>
            <div class="w3ls_gallery_grids">
                @forelse($products as $product)
                <div class="col-md-4 w3_agile_gallery_grid" style="margin-bottom: 20px;">
                    <div class="agile_gallery_grid">
                        <a>
                            <div class="agile_gallery_grid1">
                                <img src="{{asset('/storage/product/'.$product->image)}}" alt="{{$product->name}}" class="img-responsive" />
                                <div class="w3layouts_gallery_grid1_pos">
                                    <h3>{{$product->name}}</h3>
                                    <p>{{number_format($product->price, 0 , ',', '.')}} vnđ</p>
                                </div>
                            </div>
                        </a>
                        <div class="product-info text-center">
                            <a href="{{route('cart.product', $product->id)}}" class="btn btn-primary"> Thêm vào giỏ hàng</a>
                        </div>
                    </div>
                </div>
                @empty
                    <div><h2 class="text-center">Không có dữ liệu</h2></div>
                @endforelse
                <div class="clearfix"></div>
                <!-- pagination -->
                <div class="row text-center">
                    {{ $products->links() }}
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- //gallery -->
@endsection