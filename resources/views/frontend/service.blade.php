@extends('frontend.layouts.app')
@section('content')
    <!-- about-top -->
    <div class="agileits-about-top">
        <div class="container">
            <h3 class="heading-agileinfo">Services<span>Skin,Nails and Beauty Services</span></h3>
            <div class="agileinfo-top-grids">
                @forelse($services as $service)
                <div class="col-sm-4 wthree-top-grid">
                    <a href="{{route('frontend.service.detail', $service->id)}}">
                        <img src="{{asset('/storage/service/'.$service->image)}}" class="img-responsive" alt="{{$service->name}}" style="max-height: 450px;" />
                    </a>
                    <h4><a href="{{route('frontend.service.detail', $service->id)}}">{{$service->name}}</a></h4>
                    <p>{{number_format($service->price, 0 , ',', '.')}} vnđ <span class="more"><a href="{{route('book.service', $service->id)}}">Đặt ngay</a></span></p>
                </div>
                @empty
                    <div class="col-sm-12"><h2 class="text-center">Không có dữ liệu</h2></div>
                @endforelse

                <div class="clearfix"> </div>
                <!-- pagination -->
                <div class="row text-center">
                    {{ $services->links() }}
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <!-- //about-top -->
@endsection