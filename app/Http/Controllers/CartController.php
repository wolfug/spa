<?php

namespace App\Http\Controllers;

use App\Http\Requests\CartUpdateRequest;
use App\Http\Requests\OrderRequest;
use App\Language;
use App\Order;
use App\OrderDetail;
use App\Product;
use Carbon\Carbon;
use Gloudemans\Shoppingcart\Facades\Cart;

class CartController extends Controller
{

    public function cart($id)
    {
            $product = Product::select('id', 'price', 'name', 'image')
                ->where('id', trim($id))
                ->firstOrFail();
        Cart::add(['id' => $product->id, 'name' => $product->name, 'qty' => 1, 'price' => $product->price, 'options' => ['image' => $product->image]]);
        return redirect()->back()->with('alert-success', trans('messages.add_cart'));
    }

    public function all_cart()
    {
        $carts = Cart::content();

        return view('frontend.cart', ['carts' => $carts]);
    }
    public function delete($rowId)
    {
        Cart::remove($rowId);
        return redirect()->back()->with('alert-success', trans('messages.delete_cart'));

    }
    public function update($rowId, CartUpdateRequest $request) {
        Cart::update($rowId, $request->input('quantity')); // Will update the quantity
        return redirect()->back()->with('alert-success', trans('messages.update_cart'));
    }

    public function order(OrderRequest $request)
    {

        $carts = Cart::content();
        if(!count($carts) > 0) {
            return redirect()->back()->with('alert-warning', trans('messages.cart_empty'));
        }
        $order = Order::create([
            'name' =>$request->input('name'),
            'address' => $request->input('address'),
            'email' => $request->input('email'),
            'type' => Order::UNPAID,
            'total' => $request->input('price'),
            'phone' => $request->input('phone'),
            'description' => $request->input('description'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        foreach ($carts as $cart) {
            OrderDetail::create([
                'order_id' =>$order->id,
                'product_id' => $cart->id,
                'total' => $cart->qty,
                'name' => $cart->name,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
        return redirect()->back()->with('alert-success', trans('messages.cart_paid'));

    }
}
