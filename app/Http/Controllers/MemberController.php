<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Class needed for login and Logout logic
use Illuminate\Foundation\Auth\AuthenticatesUsers;

//Auth facade
use Auth;
use App\Customer;

class MemberController extends Controller
{
//    Auth::guard('member')->check()
// Auth::guard('member')->user()->email;
    use AuthenticatesUsers;

    protected $redirectTo = '/';

    //Custom guard for member
    public function __construct()
    {
        $this->middleware('guest:member')->except('logout');
    }

    //Shows member login form
    public function showLoginForm()
    {
        return view('frontend.auth.login');
    }

    public function postLogin(Request $request)
    {
        // Attempt login
        if (Auth::guard('member')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            return redirect()->intended(route('frontend.index'));
        } else {
            //Else redirect to form login
            return redirect()->back()->withInput($request->only('email', 'remember'));
        }
    }

    public function getRegister() {

    }
}
