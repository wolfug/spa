<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;

class ServiceController extends Controller
{
    public function index()
    {
        $services = Service::select('id', 'name', 'image', 'price', 'description')
            ->orderBy('updated_at', 'DESC')
            ->paginate(DEFAULT_PAGINATION_PER_PAGE);
        return view('frontend.service', ['services' => $services]);
    }

    public function detail($id)
    {
        $service = Service::select('id', 'name', 'image', 'price', 'description')
            ->where('id',$id)
            ->firstOrFail();
        return view('frontend.service_detail', ['service' => $service]);
    }
}
